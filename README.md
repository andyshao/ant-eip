
<h1 align="center">Vue Antd Admin  界面样式修改</h1>

<div align="center">
[Ant Design Pro](https://github.com/ant-design/ant-design-pro) 的 Vue 实现版本  
开箱即用的中后台前端/设计解决方案

![image](./src/assets/img/1.png)
![image](./src/assets/img/2.png)
![image](./src/assets/img/3.png)
![image](./src/assets/img/4.png)
![image](./src/assets/img/5.png)
</div>

- 预览地址：http://ant.eipflow.com/   admin 888888
系统参考界面

![image](./src/assets/img/6.png)
![image](./src/assets/img/7.png)
![image](./src/assets/img/8.png)
![image](./src/assets/img/9.png)
![image](./src/assets/img/10.png)
![image](./src/assets/img/11.png)
![image](./src/assets/img/12.png)
- 预览地址：http://netauth.eipflow.com/      admin  123456
### clone
```bash
$ git clone https://gitee.com/sunzewei/ant-eip.git
```
### yarn
```bash
$ yarn install
$ yarn serve
```
### or npm
```
$ npm install
$ npm run serve
```
更多信息参考 [使用文档](https://iczer.gitee.io/vue-antd-admin-docs)

联系QQ  1039318332
